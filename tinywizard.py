import re
import argparse
import subprocess

parser = argparse.ArgumentParser(description='Process inputs')

parser.add_argument('ifile', help='Path of the input file.')
args = parser.parse_args()


file = open(args.ifile,'r')
fichier = file.read()
file.close()

## Search all packages imported:

packages = re.findall(r'\\usepackage{(\S+)}',fichier)

for package in packages:
	print("Searching {}".format(package))
	
	command = subprocess.Popen(["tlmgr","search","--global","--file","/{}.sty".format(package)], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	out, err = command.communicate()

	out = out.decode("utf-8").splitlines()
	to_install = re.findall(r'\\n(\S+):',out)

	print("Installing {}".format(to_install))

	command = subprocess.Popen(["tlmgr","install","{}".format(to_install)], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	out, err = command.communicate()


print("Done.")